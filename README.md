# pasaplata-app

Pasaplata App is a web App that connected to a bitcoin exchange and provided live quotes for making remittances between Chile and Colombia. It depended on the surbtc-rest-client and remittance-maker modules as well as a backend server.
The Live demo has been down due to a change on API endpoints and lack of maintenance.  


This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.14.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.
